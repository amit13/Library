<?php include('header.php'); ?>
<?php include('navbar.php'); ?>
    <div class="container">
		<div class="margin-top">
			<div class="row">
					<?php include('head.php'); ?>
				
				</div>
				<div class="span10">
					<?php include('slider.php'); ?>
				</div>
			
				<div class="span2">
				<h4></h4>
			
				</div>
				<div class="span10">
				
				
				<?php include('thumbnail.php'); ?>
				
					
					<div class="text_content">
					<div class="abc">
					<!-- text content -->
					<h4>Vision</h4>
					<ul>
                   	  <li>To achieve high student performance and overall organizational learning by continuously upgrading technical knowledge and enhancing professional as well as teaching skills of faculties.</li>
                      <li> To provide state-of-the-art infrastructure and services to the students and staff. </li>
                      <li>To engage in active research and technical consultancy.</li>
                      <li>To forge mutually beneficial relation with government organizations, industries, alumni and society at large.</li>
                   </ul>
					<hr>
					
					<hr>
					</div>
					</div>
					<!-- end content -->
				
				
				</div>
			
			</div>
		</div>
    </div>
<?php include('footer.php') ?>